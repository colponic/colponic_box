## Bienvenido al repositorio  Colponic box 
En este repositorio encontrara los planos y archivos STL  de  la caja de protección de PCB y caja de protección sensores.

**Instrucciones:**

- Si desea utilizar los archivos STL , debe tener el Software Cura 4.8.0 para modificar los parámetros de impresión y ajustarlos  a su impresora 3D, del mismo modo si cambia el material.
- Los planos pueden ser abiertos con cualquier lector de pdf.

**Caja de protección de PCB**

![alt text](./image/cajapcb.jpg)

**Caja de protección sensores**

![alt text](./image/cajasensor.jpg)
